import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SwingTest {

    private static final String TITLE = "Hide the square";
    private static final String MESSAGE = "Just drag the square to hide it!";
    private int height = 500, width = 500;
    private float kw, kh;
    private static final int PANEL_WIDTH = 100;
    private static final int PANEL_HEIGHT = 100;
    private Point exPoint;
    private JPanel panel;
    private JFrame frame;

    private SwingTest () {
        frame = new JFrame(TITLE);
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(width, height);

        frame.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {

                height = frame.getContentPane().getHeight();
                width = frame.getContentPane().getWidth();

                kh = (height - PANEL_HEIGHT)/255.0f;
                kw = (width - PANEL_WIDTH)/255.0f;

            }

            @Override
            public void componentMoved(ComponentEvent e) {

            }

            @Override
            public void componentShown(ComponentEvent e) {

            }

            @Override
            public void componentHidden(ComponentEvent e) {

            }
        });


        panel = new JPanel();
        panel.setOpaque(true);
        panel.setBackground(Color.RED);

        MoveListener ml = new MoveListener();
        panel.addMouseListener(ml);
        panel.addMouseMotionListener(ml);

        panel.setSize(PANEL_WIDTH, PANEL_HEIGHT);
        panel.setLocation(0, 0);

        frame.add(panel);

        JOptionPane.showMessageDialog(frame, MESSAGE);

        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new SwingTest();
    }



    class MoveListener extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            exPoint = e.getPoint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            super.mouseDragged(e);


            panel.setLocation(panel.getX() + e.getX() - (int)exPoint.getX(),
                    panel.getY() + e.getY() - (int)exPoint.getY());


            if(panel.getBounds().x < 0){
                panel.setLocation(0, panel.getY());
            }

            if(panel.getBounds().x > width - PANEL_WIDTH){
                panel.setLocation(width - PANEL_WIDTH, panel.getY());
            }

            if(panel.getBounds().y < 0){
                panel.setLocation(panel.getX(), 0);
            }

            if(panel.getBounds().y > height - PANEL_HEIGHT){
                panel.setLocation(panel.getX(), height - PANEL_HEIGHT);
            }

            panel.setBackground(new Color(255, colorGreen(), colorBlue()));

        }

    }

    private int colorGreen() {

        return Math.round(panel.getBounds().x / kw);

    }

    private int colorBlue() {

        return Math.round(panel.getBounds().y / kh);

    }

}

